<?php

echo "Below is a list of the next 20 leap years: <br /><br />";

$currentYear = new \DateTime("2016/8/2");
$leapYearInterval = new \DateInterval('P1Y');
$leapYearPeriod = new \DatePeriod($currentYear,$leapYearInterval,80);
foreach ($leapYearPeriod as $date) 
{
	if ((bool)$date -> format('L')) 
	{
		echo $date -> format('Y') . "<br/> <br />" ;
	}
}

?>
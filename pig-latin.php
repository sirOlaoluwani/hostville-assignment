<?php
$original = "This guy is a thief";
$say = explode(" ", $original);

$pigLatin = "";
foreach ($say as $words) 
{
	$words = trim($words);
	$firstLetter = substr($words, 0,1);
	$remaining = substr($words, 1,strlen($words)-1);
    
    if (trim($words)) 
    {
    	$pigLatin . = (strlen($words) == 1)?$firstLetter . " " :$remaining.$firstLetter. "ay ";
    }
}

echo $original . " becomes: " . $pigLatin;
?>